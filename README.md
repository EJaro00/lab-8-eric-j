Tasks to answer in your own README.md that you submit on Canvas:

1.  In the logger.properties file where the logger is configured, the consolehandler is filtered to the INFO level, meaning it will only print INFO messages, whereas the FileHandler's filter is kept on ALL, meaning all log messages will be written to logger.log.
1.  That line comes from the external maven library org.junit.jupiter.engine.execution.ConditionEvaluator which contains its own logger with a FINER message.
1.  Assertions.assertThrows passes if the function called throws the provided exception, and fails if the function does not throw the exception.
1.  See TimerException and there are 3 questions
    1.  A serialVersionUID is a version number for a serializable class that verifies that the sender and reciever of a serialized object have  loaded classes for that object that are compatible with respect to serialization. If we don't explicitly create one, serialization runtime will create a default serialVersionUID which is incredibly sensitive to class details that can vary based on machine compilers, which could lead to many InvalidClassExceptions being thrown during deserialization.
    2.  Subclasses in Java do not inherit constructors from their superclasses, so we need to declare new constructors inside our subclasses that specifically call the constructors for the superclasses.
    3.  Subclasses in Java inherit methods from their superclasses, so we do not need to go through and override every exception method with a new method that simply calls the super method.	
1.  The Timer.java's static{} block is executed only once whenever the first Timer object is created, and sets up the configuration for every timer's configFile, and tells the user either that the program is starting up, or that the config file was not opened.
1.  MD is short for Markdown, which is a text formatting software used by Bitbucket's servers.
1.  The timeMe function has the initial data for timeNow immediately after throwing the TimerException, so the initialization for timeNow would never occur, and as such the failed test would appear to throw a NullPointerException when timeNow was accessed later, rather than a TimerException.
1.  Once the TimerException is thrown, the program exits the try block only to find there is no catch block for TimerExceptions. It then goes to the "finally" block where it attempts to access data from the never initialized timeNow, which throws a NullPointerException.
1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
1.  Make a printScreen of your eclipse Maven test run, with console
1.  TimerException is a checked exception (Exception) and NullPointerException is an unchecked exception (RuntimeException).
1.  Push the updated/fixed source code to your own repository.